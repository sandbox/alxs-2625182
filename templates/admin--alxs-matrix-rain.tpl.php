<?php

/**
 * @file
 * Template for the admin configuration form.
 *
 * The admin configuration form allows for setting of various
 * different parameters associated with the display of the block.
 */
?>
<h2>Colors</h2>
<hr><br>
<p><?php print t("Setup the color scheme for the matrix code rain here. Some of these settings cannot be maniuplated using standard CSS style sheets, so we have included every setting here for any colors associated with the code rain block.") ?></p>
<?php print render($form['alxs_matrix_background_color']); ?>
<?php print render($form['alxs_matrix_text_color']); ?>
<?php print render($form['alxs_matrix_shadow_color']); ?>
<?php print render($form['alxs_matrix_code_color']); ?>
<h2>Content</h2>
<hr><br>
<p><?php print t("Setup the content here. These are settings for the content in the code rain block including the optional overlay.") ?></p>
<?php print render($form['alxs_matrix_secret_code']); ?>
<?php print render($form['alxs_matrix_show_content']); ?>
<?php print render($form['alxs_matrix_text_content']); ?>
<h2>Parameters</h2>
<hr><br>
<p><?php print t("Setup other optional parameters here. These would be anything associated with the functionality of the code rain, including velocity, length and column sizing properties.") ?></p>
<?php print render($form['alxs_matrix_min_length']); ?>
<?php print render($form['alxs_matrix_max_length']); ?>
<?php print render($form['alxs_matrix_min_velocity']); ?>
<?php print render($form['alxs_matrix_max_velocity']); ?>
<?php print render($form['alxs_matrix_column_width']); ?>
<?php print render($form['alxs_matrix_column_height']); ?>
<hr><br>
<?php print render($form['alxs_matrix_submit']); ?>
<?php hide($form['alxs_matrix_submit']); ?>
<?php print drupal_render_children($form); ?><?php print drupal_render_children($form); ?>
