-- SUMMARY --

The ALXS Matrix Rain module creates a flly-customizable block 
for displaying an effect similar to that seen on the popular film 
'The Matrix'.

There are many configuration options, all found under 
'Admin > Configuration > User Interface > Matrix Rain'. 
There are some parameters stored here which CAN NOT be set using 
plain CSS and MUST BE SET HERE. One of these options is the color 
of the actual code rain.

For a full description of the module, visit the project page:
  http://drupal.org/project/alxs_matrix

To submit bug reports and feature suggestions, or to track changes:
  http://drupal.org/project/issues/alxs_matrix


-- REQUIREMENTS --

jQuery Colorpicker >> https://www.drupal.org/project/
jquery_colorpicker


-- INSTALLATION --

This module depends on libraries API, then you have to move the 
JQuery library from the module folder to libraries folder. 

* Install http://drupal.org/project/libraries
* Install this module just as you would any other Drupal module
* Download https://github.com/ALXS-Design/JQuery-Matrix-Rain/archive/master.zip
* Extract the the zip file content and put copy the JS folder to 
  [path to libraries folder]/alxs_matrix/

-- CONTACT --

Current maintainers:

* Alex Scott - https://www.drupal.org/u/alxs
